import config from '../config';
import fetch from 'node-fetch';
import {Continents} from '../model/Continents';
import {Countries} from '../model/Countries';
import {States} from '../model/States';

export default class GetCountryData{

    constructor(props) {

        this.baseUrl = config.countryURL;

    }

    async getContients(){
        console.log('getContients started! 🚀')
        const response = await fetch(`${this.baseUrl}continents/`);
        const results = await response.json();
        const {_links} =  results;


        for(const link of _links['continent:items']){

            const {href} = link;


            const countryData = await fetch(href);
            const countryResults = await countryData.json();

            const {name,geonames_code} =  countryResults;

            const continent = new Continents();

            const found = await Continents.findOne({ name });

            if(found) continue;

            continent.name = name;
            continent.geonames_code = geonames_code;
            await continent.save();

        }

        console.log('getContients done! 🚀')


    }

    async getCountries() {
        console.log('getCountries started! 🚀')
        const continents = await Continents.find();


        for (const contient of continents) {
            const {geonames_code} = contient;
            const response = await fetch(`${this.baseUrl}continents/geonames:${geonames_code}/countries`);
            const results = await response.json();
            const {_links} = results;

            for (const link of _links['country:items']) {

                const {href} = link;


                const countryData = await fetch(href);
                const countryResults = await countryData.json();

                const {
                    name,
                } =  countryResults;

                const found = await Countries.findOne({ name });
                if(found) continue;
                const country = await Countries.create(countryResults)
                contient.countries.push(country)
                await country.save();
                await contient.save();

            }

        }
        console.log('getCountries done! 🚀')
    }

    async getStateOrProvince() {
        console.log('getStateOrProvince started! 🚀')

        const countries = await Countries.find();

        for (const country of countries) {

            const {iso_alpha2} = country;
            const response = await fetch(`${this.baseUrl}countries/iso_alpha2:${iso_alpha2}/admin1_divisions`);
            const results = await response.json();
            const {_links} = results;

            for(const link of _links['a1:items']) {
                const {href} = link;

                const stateData = await fetch(href);
                const stateResults = await stateData.json();

                const {
                    name,
                } =  stateResults;



                const found = await States.findOne({ name });

                if(found || !name) continue;

                const state = await States.create(stateResults)

                country.states.push(state)

                await country.save();
                await state.save();

            }

        }

        console.log('getStateOrProvince done! 🚀')
    }

    async run(){

        console.log('Begin API extraction')
        await this.getContients();
        await this.getCountries();
        await this.getStateOrProvince();



    }





}
