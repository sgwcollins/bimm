import { gql } from 'apollo-server-express';

import continentsSchema from './continents';
import countriesSchema from './countries';
import stateSchema from './states';

const linkSchema = gql`
    scalar Date
    type Query {
        _: Boolean
    }
`;

export default [
    linkSchema,
    continentsSchema,
    countriesSchema,
    stateSchema,
];
