import { gql } from 'apollo-server-express';

export default gql`
    extend type Query {
        states: [States!]
        state(id: ID!): States
    }
    
    type States {
        id: ID!
        name: String!
    }
`;
