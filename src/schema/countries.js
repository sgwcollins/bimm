import { gql } from 'apollo-server-express';

export default gql`
    extend type Query {
        countries: [Countries!]
        country(id: ID!): Countries
    }
    
    type Countries {
        id: ID!
        name: String!
        iso_alpha2: String!
        iso_alpha3: String!
        population: String!
        states: [States!]
    }
`;
