import { gql } from 'apollo-server-express';

export default gql`
    extend type Query {
        continents: [Continents!]
        continent(id: ID!): Continents
    }
    
    type Continents {
        id: ID!
        name: String!
        geonames_code: String!
        countries: [Countries!]
    }
`;
