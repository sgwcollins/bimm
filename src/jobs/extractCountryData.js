import GetCountryData from '../services/getCountryData';

export default class ExtractCountryData {

    async handler(job, done){
        const countryService = new GetCountryData();
        await countryService.run();
        done();
    }
}
