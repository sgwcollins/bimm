import mongoose from "mongoose";

const {Schema} = mongoose;

export const Countries = mongoose.model('Countries',
    {
        name: {
            type:String,
            required:true,
            unique:true
        },
        currency_code: {
            type:String,
        },
        iso_alpha2: {
            type:String,
            required:true,
            unique:true
        },
        iso_alpha3: {
            type:String,
            required:true
        },
        population: {
            type:Number,
            required:true
        },
        continent:{
                type: Schema.Types.ObjectId,
                ref: 'Continents'
        },
        states: [{
                type: Schema.Types.ObjectId,
                ref: 'States'
            }
        ]

    }
);
