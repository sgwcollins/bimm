import mongoose from "mongoose";

const {Schema} = mongoose;

export const Continents = mongoose.model('Continents',
    {
        name: {
            type:String,
            required:true,
            unique: true
        },
        geonames_code: {
            type:String,
            required:true,
            unique: true
        },
        countries: [
            {
                type:Schema.Types.ObjectId,
                ref:'Countries'
            }
        ]
    }
);
