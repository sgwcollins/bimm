import mongoose from "mongoose";

const {Schema} = mongoose;

export const States = mongoose.model('States',
    {
        name: {
            type:String,
            required:true
        },
        countries: {
            type: Schema.Types.ObjectId,
            ref: 'Countries'
        }
    }
);
