import dotenv from 'dotenv';
// config() will read your .env file, parse the contents, assign it to process.env.
dotenv.config();

const envFound = dotenv.config();
if (!envFound) {
    // This error should crash whole process

    throw new Error("⚠️  Couldn't find .env file  ⚠️");
}

export default {

    databaseURL: process.env.MONGODB_URI,
    countryURL: process.env.GET_CONTINENTS_BASE_URL,

}
