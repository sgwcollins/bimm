

import {Continents} from '../model/Continents';
import {Countries} from '../model/Countries';
import {States} from '../model/States';


export default {
    Query: {
        countries: async (parent, args, { models }) => {
            return await Countries.find();
        },
        country: async (parent, { id }, { models }) => {
            return await Countries.findById(id);
        },
    },


    Countries: {
        states: async (country, args, { models }) => {
            return await States.find({
                _id: country.states,
            });
        },
    },
};
