import {Continents} from '../model/Continents';
import {Countries} from '../model/Countries';



export default {
    Query: {
        continents: async () => {
            return await Continents.find();
        },
        continent: async (parent, { id }, { models }) => {
            return await Continents.findById(id);
        },
    },


    Continents: {
        countries: async (continent) => {
            return await Countries.find({
                _id: continent.countries,
            });
        },
    },
};
