
import {Continents} from '../model/Continents';
import {Countries} from '../model/Countries';
import {States} from '../model/States';


export default {
    Query: {
        states: async (parent, args, { models }) => {
            return await States.find();
        },
        state: async (parent, { id }, { models }) => {
            return await States.findById(id);
        },
    },

};
