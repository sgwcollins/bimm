import { GraphQLDateTime } from 'graphql-iso-date';

import countriesResolvers from './countries';
import stateResolvers from './states';
import continentsResolvers from './continents';

const customScalarResolver = {
    Date: GraphQLDateTime,
};

export default [
    customScalarResolver,
    countriesResolvers,
    stateResolvers,
    continentsResolvers,
];

