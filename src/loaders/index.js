import mongooseLoader from './mongoose';
import agendaLoader from './agenda'
import jobsLoader from './job'
import GetCountryData from '../services/getCountryData'

import {Continents} from '../model/Continents';
import {Countries} from '../model/Countries';
import {States} from '../model/States';


export default async () => {
    const mongoConnection = await mongooseLoader();

    /* For debug purposes */
    // await Promise.all([
    //     Continents.deleteMany({}),
    //     Countries.deleteMany({}),
    //     States.deleteMany({}),
    // ]);


    const agenda = await agendaLoader(mongoConnection);

    await jobsLoader(agenda);

    await new GetCountryData().run();


}
