import ExtractCountryData from '../jobs/extractCountryData';
import Agenda from 'agenda';
import config from '../config'

export default async () => {

    const agenda = new Agenda({db: {address: config.databaseURL}});

    agenda.define(
        'extract country data',
        {priority: 'high', concurrency: 20},
        new ExtractCountryData().handler,
    );


    await agenda.start();
    await agenda.every('1 week', 'extract country data');


}
