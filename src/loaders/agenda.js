import Agenda from 'agenda';


export default ({ mongoConnection }) => {
    return new Agenda({
        mongo: mongoConnection,
        processEvery: '30 seconds',
    });
}
