import  { ApolloServer } from 'apollo-server'
import mongoose from 'mongoose';
import resolvers from './resolvers'
import schema from './schema';
import express from 'express'
import {loaders} from './loaders';
const PORT = 8080;

export const startServer = async () =>{


  const server = new ApolloServer(
      {
        typeDefs: schema,
        resolvers,
        formatError: error => {
          // remove the internal sequelize error message
          // leave only the important validation error
          const message = error.message
              .replace('SequelizeValidationError: ', '')
              .replace('Validation error: ', '');

          return {
            ...error,
            message,
          };
        },
        context: async ({req, connection}) => {
          if (connection) {
            return {
              models,
              loaders: {
                user: new DataLoader(keys =>
                    loaders.user.batchUsers(keys, models),
                ),
              },
            };
          }
        }
      }
  )
    server.listen(PORT).then(({ url }) => {
        console.log(`🚀  Server ready at ${url}`);
    });

    await require('./loaders').default();



}

startServer();
